package com.jiushi6.officeonline.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Project: officeonline
 * @Package com.jiushi6.officeonline.service
 * @Description: 文件服务接口
 * @author: 飞翔的荷兰人
 * @date Date : 2021年01月07日 下午 21:14
 */
public interface IFileService {

    /**
     * 文件上传接口
     * @param file 文件
     * @return 操作结果
     */
    boolean upload(MultipartFile file);

    /**
     * 文件下载接口
     * @param fileId 文件ID
     */
    void  download(String fileId, HttpServletRequest request, HttpServletResponse response);
}
