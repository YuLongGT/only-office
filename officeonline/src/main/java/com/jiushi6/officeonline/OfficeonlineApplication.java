package com.jiushi6.officeonline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfficeonlineApplication {

    public static void main(String[] args) {
        SpringApplication.run(OfficeonlineApplication.class, args);
    }

}
